#include "floatlist.h"

int main() {
    floatList list;
    list.appendNode(2.5);
    list.appendNode(7.9);
    list.appendNode(12.6);
    list.appendNode(5.8);
    list.displayList();

    list.deleteNode(7.9);
    std::cout << "7.9 is deleted" << std::endl;
    list.displayList();

    list.deleteNode(2.5);
    list.appendNode(6.2);
    list.displayList();

    list.deleteNode(6.2);
    list.displayList();

    return 0;
}